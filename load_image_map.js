/**
 * @file
 * Attach a responsive image map to an image.
 *
 * Called from <img onload /> with argument this.
 * Calculates the path to the image map from the img src property by
 *   replacing the file extension (png, jpg, gif) with 'cmapx'.
 * Reads the image map and appends it to the <img /> element.
 * Recalculates the map when the browser window is resized.
 */

(function ($) {

  'use strict';
  Drupal.behaviors.loadImageMap = function (img) {
    if (!img.src) {
      return;
    }

    // Find the image map for this image by convention.  The image map
    // is in a file in the same directory with extension cmapx.
    var path = /(.*)\.[^\.]*$/;

    // The img is a DOM object containing the image.
    var mapUrl = img.src.replace(path, '$1.cmapx');

    // Processing if the cmapx file is successfully found
    var fsuccess = function (data) {

      // Image map file successfully loaded into data so add to DOM.
      var $img = $(img);
      $img.append(data);

      // The image we just loaded now contains its image map.
      // Tell the image to use the map.
      var $map = $img.children('map');
      var mapId = $map.attr('id');
      $img.attr('usemap', '#' + mapId);

      // Save the 'natural' coordinate values in a new attribute
      // attached to each area element.
      $map.find('area').each(function (index, elem) {
        var coords = $(this).attr('coords');
        $(this).attr('naturalCoords', coords);
      });

      // Function to rescale the image map when window is resized.
      var resize = function () {

        // Calculate scale factors for x, y resize.
        var $img = $(img);

        // Get the dimensions of the image independent of jQuery release.
        // Before jQuery 1.7 dimensions were in attributes.  From 1.7 on
        // dimensions are in properties.  Thanks a bunch jQuery team.
        var width;
        var naturalWidth;
        var height;
        var naturalHeight;
        if ('prop' in $img) {
          width = $img.prop('width');
          naturalWidth = $img.prop('naturalWidth');
          height = $img.prop('height');
          naturalHeight = $img.prop('naturalHeight');
        }
        else {
          width = $img.attr('width');
          naturalWidth = $img.attr('naturalWidth');
          height = $img.attr('height');
          naturalHeight = $img.attr('naturalHeight');
        }
        var xscale = width / naturalWidth;
        var yscale = height / naturalHeight;

        // Scale the x, y coords for each area.
        $map.find('area').each(function (index, elem) {
          var ncoords = $(this).attr('naturalCoords').split(',');
          var scoords = String('');
          var first = true;
          for (var i = 0; i < ncoords.length; i++) {
            if (first) {
              first = false;
            }
            else {
              scoords = scoords.concat(',');
            }
            var x = ncoords[i++] * xscale;
            var y = ncoords[i] * yscale;
            scoords = scoords.concat(x, ',', y);
          }
          $(this).attr('coords', scoords);
        });
      };
      window.onresize = resize;

      // Trigger a resize event to cause initial scaling.
      $(window).resize();
    };

    $.ajax({
      type: 'GET',
      url: mapUrl,
      success: fsuccess
    });
  };
}(jQuery));
