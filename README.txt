
                       Load Image Map module

Load a client-side image map from the server, attach it to the image
and scale the map responsively.

Function:

Graphviz produces image files with a client-side image map in a
separate file. By convention, if the image file is named
some-image.png (or some-image.jpg or some-image.gif) then the image
map file is named some-image.cmapx . The cmapx file has a simple
format which could be easily created by hand or by other software.

This module uses JavaScript to attach a client-side image map to an
image by loading the cmapx file from the same directory as the image
file. If no such cmapx file exists or JavaScript is not enabled in the
browser, no error is signaled and the page loads normally.

After the image map is attached to the image, changes in the size of
the browser window are detected and the image map is modified to
match, thus making the image map responsive to browser size.

Installation:

Install and enable this module in the usual way. Wherever you load an
image which has (or could have) an image map, edit the HTML <img ...>
referencing the image to add
onload="Drupal.behaviors.loadImageMap(this)" . Store the image map in
a file in the same directory as the image file with the same name as
the image file but replace the extension jpg (gif, png) with cmapx.

Related:

    The Responsive Image Maps module adjusts an image map stored in
    the page to respond to changes in browser window size.
    
    The Graphviz module implements a Graph API hook for views integration.

    The Graphviz Filter module treats input text as Graphviz DOT
    syntax, converts it using Graphviz tools to the requested
    format(s) and renders it in HTML.

    The Graphviz Noderef module displays nodes as a Graphviz
    graph/map, with NodeReference fields as edges between nodes.
